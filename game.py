from random import randint

# Asks human for their name with "Hi! What is your name?"
name = input("Hi! What is your name?")
# Does the following up to five times
for guess_number in range(5):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

# Guess your birth month and year with format
    print("Guess", guess_number+1, name, "were you born in",
    month_number, "/", year_number, "?")

    response = input("yes or no?")

    if response == "yes":
        print("I knew it!")
        break
    elif response == "no" and guess_number+1 < 5:
        print("Drat! Lemme try again!")
    else:
        print("I have other things to do. Good bye.")
